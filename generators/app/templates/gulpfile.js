var gulp = require('gulp'),
    uglify = require("gulp-uglify"),
    concat = require("gulp-concat"),
    sass = require('gulp-sass'),
    neat = require('node-neat').includePaths,
    cssmin = require('gulp-cssmin'),
    rename = require('gulp-rename'),
    watch = require('gulp-watch'),
    plumber = require('gulp-plumber'),
    babel = require("gulp-babel"),
    browserSync = require('browser-sync').create(),
    ejs = require("gulp-ejs"),
    paths = {
        css_dir:  './app/',
        js_dir:  './app/js/',
        cssmin_dir: './app/css/min/',
        js_main_dir:  './app/js/main/min/',
        ejs_dir: './app/ejs/',
        html_dir: './app/html/',
        scss: './app/scss/*.scss',
        css:  './app/*.css',
        js:   './app/js/**.js',
        html: './app/html/**.html',
        ejs: './app/ejs/**.ejs',
        js_main: './app/js/js.js',
        babel: './app/js/babel/*.es6'
    };
gulp.task('ejs', function() {
    gulp.src( [paths.ejs,'!' + paths.ejs_dir + "/**/_*.ejs"])
    .pipe(ejs({}, {ext:'.html'}))
    .pipe(gulp.dest(paths.html_dir))
     .pipe(browserSync.stream());
});

gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./app/html/"
        }
    });
});
gulp.task('bs-reload', function () {
    browserSync.reload();
    console.log('(´ ºムº `) RELOAD');
});

gulp.task('styles', function () {
    return gulp.src(paths.scss)
        .pipe(plumber({
            errorHandler: function(err) {
            console.log(err.messageFormatted);
            this.emit('end');
            }
        }))
        .pipe(sass({
            includePaths: [paths.scss].concat(neat)
        }))
        .pipe(gulp.dest(paths.css_dir));
});
gulp.task('babel', function() {
    console.log('(´ ºムº `) Go Babel');
    gulp.src(paths.babel)
        .pipe(plumber())
        .pipe(babel({
            presets:['es2015']
        }))
        .pipe(rename({suffix: '.babel'}))
        .pipe(gulp.dest(paths.js_dir))
});

    gulp.task('js.concat', function() {
    console.log('(´ ºムº `) JS Concat');
    return gulp.src(paths.js)
        .pipe(plumber())
        .pipe(concat('main.js'))
        .pipe(gulp.dest(paths.js_main_dir));
});

gulp.task('js.uglify', function() {
    console.log('(´ ºムº `) JS minify');
    return gulp.src(paths.js_main)
        .pipe(plumber())
        .pipe(uglify())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(paths.js_main_dir));
});

gulp.task('css.min', function() {
    console.log('(´ ºムº `) CSS minify');
    return gulp.src(paths.css)
        .pipe(plumber())
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(paths.cssmin_dir));
});

gulp.task('minify', function () {
    gulp.start(['css.min'],['js.uglify']);
});

gulp.task('watch',['browser-sync'], function () {
    gulp.watch( paths.scss , ['styles']);
    // gulp.watch( paths.css , ['css.min']);
    // gulp.watch( paths.js_main, ['js.uglify']);
    gulp.watch( paths.js, ['js.concat']);
    gulp.watch( './js/babel/*.es6', ['babel']);
    // gulp.watch(paths.scss, ['bs-reload']);
    // gulp.watch(paths.js, ['bs-reload']);
    // gulp.watch(paths.html, ['bs-reload']);
    gulp.watch(paths.ejs, ['ejs']);
//    gulp.watch(paths.ejs, ['bs-reload']);
//gulp.watch("./app/html/*.html", ['htmlTasks', browserSync.reload]);
    gulp.watch(paths.html).on('change', browserSync.reload);


});

gulp.task('default',function(){
    gulp.start('watch');
});
