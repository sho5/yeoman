'use strict';
var yeoman = require('yeoman-generator');
var chalk = require('chalk');
var yosay = require('yosay');

module.exports = yeoman.Base.extend({
    prompting: function () {
        // Have Yeoman greet the user.
        this.log(yosay(
            'Welcome to the tremendous ' + chalk.red('generator-basic') + ' generator!'
        ));

        var prompts = [{
            type: 'confirm',
            name: 'someAnswer',
            message: 'Would you like to enable this option?',
            default: true
        },
        {
            name: 'yourname',
            message: 'What is your name?',
            default: "someuser"
        },
        {
            name:"bodySize",
            message:"What is your 3 size ?",
            default: "none-data"
        },
    ];
    return this.prompt(prompts).then(function (props) {
        // To access props later use this.props.someAnswer;
        this.someAption = props.someAnswer;
        this.yourname = props.yourname;
        this.bodySize = props.bodySize;
    }.bind(this));
    console.log(prompts);
},
writing: function () {
    var yourname = this.yourname;
    // this.mkdir('app');
    this.fs.copyTpl(
        this.templatePath('_package.json'),
        this.destinationPath('package.json'),
        {yourname: yourname}
    );
    this.fs.copyTpl(
        this.templatePath('html/index.html'),
        this.destinationPath('app/html/index.html'),
        {yourname: yourname,bodySize:this.bodySize}
    );
    this.fs.copyTpl(
        this.templatePath('gulpfile.js'),
        this.destinationPath('gulpfile.js'),
        {yourname: yourname,bodySize:this.bodySize}
    );
    this.fs.copyTpl(
        this.templatePath('ejs/index.ejs'),
        this.destinationPath('app/ejs/index.ejs'),
        {yourname: yourname,bodySize:this.bodySize}
    );
    this.fs.copyTpl(
        this.templatePath('ejs/_footer.ejs'),
        this.destinationPath('app/ejs/_footer.ejs'),
        {yourname: yourname}
    );
    this.fs.copyTpl(
        this.templatePath('ejs/_header.ejs'),
        this.destinationPath('app/ejs/_header.ejs'),
        {yourname: yourname}
    );
    this.fs.copyTpl(
        this.templatePath('scss/style.scss'),
        this.destinationPath('app/scss/style.scss'),
        {yourname: yourname}
    );
    this.log(yosay('Yo!'));
    // this.fs.copy(
    //    this.template('index.html', 'index.html'),
    //   {yourname: yourname}
    // );
},

install: function () {
       this.installDependencies();
}
});
